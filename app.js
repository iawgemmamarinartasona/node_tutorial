//app.js
var express = require('express');
var app = express();

/*
app.get('/', function (req, res) {
  res.send('Hello World!');
});
*/
const index = require('./routes/index');
const path = __dirname + '/views/';

//Que capti nomes els htmls
app.set('view engine', 'html');
//Que agafi els strings i els passi a un format que capti la url
app.use(express.urlencoded({ extended: true }));
//totes les rutes començaran a partir de path.
app.use(express.static(path));

app.use('/', index);

app.listen(3000, function() {
    console.log('Example app listening on port 3000!');
});


mongoose = require('mongoose'),
    http = require('http');

mongoose.connect(
    `mongodb://root:pass12345@localhost:27017/tutorial?authSource=admin`, { useNewUrlParser: true },
    (err, res) => {
        if (err) console.log(`ERROR: connecting to Database.  ${err}`);
        else console.log(`Database Online: ${process.env.MONGO_DB}`);
    }
);