/*
  /routes/index.js
*/
const express = require('express');
const router = express.Router();
const path = require('path');

//Middleware para mostrar datos del request
router.use(function(req, res, next) {
    console.log('/' + req.method);
    next();
});

//Analiza la ruta y el protocolo y reacciona en consecuencia
router.get('/', function(req, res) {
    res.sendFile(path.resolve('views/index.html'));
});


var ctrlDir = path.resolve("controllers/");
//Importamos el controllador
var userCtrl = require(path.join(ctrlDir, "user"));

//Link routes and functions

router.post('/mayor', userCtrl.add);
router.post('/mayor', userCtrl.isMayor);


module.exports = router;