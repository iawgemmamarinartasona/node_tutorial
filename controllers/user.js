//controllers/user.js

var User = require('../models/user');

//Metodo que almacena en DB
var add = (req, res, next) => {
    var user = new User({ name: req.body.name, age: req.body.age });
    user.save();
    req.result = user;
    next();
};
//find all people
var list = (req, res, next) => {
    User.find(function(err, users) {
        return users;
    });
};

//find person by id
var find = (req, res) => {
    User.findOne({ _id: req.params.id }, function(error, user) {
        return user;
    })
};

var isMayor = (req, res, next) => {
    if (req.body.age >= 18) {
        res.send("Major d'edat");
    } else {
        res.send("Menor d'edat");
    }
    next();
};


module.exports = {
    add,
    isMayor,
    list,
    find
}